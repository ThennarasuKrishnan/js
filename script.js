function validate()
{
    var regex_name = /^[A-Z]{2,20}$/i
    var regex_email= /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,6})+$/
    var regex_dob=/^(0?[1-9]|[12][0-9]|3[01])[\/\-](0?[1-9]|1[012])[\/\-]\d{2}$/gm
    var regex_mob=/^(?:[0-9\+]{0,2}?)[0-9]{10,11}$/
    var regex_a1=/^[a-zA-z,0-9 ]{2,40}$/
    var regex_city=/^[a-zA-Z]{3,25}$/
    var regex_zip=/^[1-9][0-9]{5}$/
    var regex_number=/[0-9]{1,20}/
    var regex_symbol=/[\/.\}\{\\$!%&_+=^\-()#~`*@,<>.\][]/
    var regex_length=/[a-zA-Z]{1}/
    var regex_length1=/[a-zA-Z]{21,100}/

    var name=document.getElementById("name") ;
    var fname=document.getElementById("fname");
    var email=document.getElementById("email");
    var dob=document.getElementById("dob");
    var mob=document.getElementById("mob");
    var a1=document.getElementById("a1");
    var a2=document.getElementById("a2");
    var city = document.getElementById("city");
    var state= document.getElementById("state");
    var zip =document.getElementById("zip");
    var pa1=document.getElementById("pa1");
    var pa2=document.getElementById("pa2");
    var pcity = document.getElementById("pcity");
    var pstate= document.getElementById("pstate");
    var pzip =document.getElementById("pzip");
    var im=document.getElementById("pic");
    var opt=state.options[state.selectedIndex];
    var opt1=pstate.options[pstate.selectedIndex];
    
    console.log(opt.value);
    console.log(im.type);
   
    if (regex_name.test(name.value))
    {
    document.getElementById("l1").innerHTML="Valid";
    document.getElementById("l1").style.visibility="visible" ;
    document.getElementById("l1").style.color="green";
    document.getElementById("name").style.border="solid green 2px";
    
    
    }
    else if (name.value.length == 0)
    {
        document.getElementById("l1").innerHTML="Name can't be blank";
        document.getElementById("l1").style.visibility="visible";
        document.getElementById("l1").style.color="Red"   ;
        document.getElementById("name").style.border="solid red 2px";
    
        
    }
    else if(regex_number.test(name.value))
    {
        document.getElementById("l1").innerHTML="Name can't contain numbers";
        document.getElementById("l1").style.visibility="visible";
        document.getElementById("l1").style.color="red";
        document.getElementById("name").style.border="solid red 2px";
    }
    else if(regex_symbol.test(name.value))
    {
        document.getElementById("l1").innerHTML="Name can't contain symbols or special characters";
        document.getElementById("l1").style.visibility="visible";
        document.getElementById("l1").style.color="red";
        document.getElementById("name").style.border="solid red 2px";
        
    }
    else if(regex_length.test(name.value))
    {
        document.getElementById("l1").innerHTML="Name must be greater than 2 characters";
        document.getElementById("l1").style.visibility="visible";
        document.getElementById("l1").style.color="red";
        document.getElementById("name").style.border="solid red 2px";
        
    }
    else if(regex_length1.test(name.value))
    {
        document.getElementById("l1").innerHTML="Name must be lesser than 20 characters";
        document.getElementById("l1").style.visibility="visible";
        document.getElementById("l1").style.color="red";
        document.getElementById("name").style.border="solid red 2px";
        
    }
    else 
    {
        document.getElementById("l1").innerHTML="Invalid name";
        document.getElementById("l1").style.visibility="visible";
        document.getElementById("l1").style.color="red";
        document.getElementById("name").style.border="solid red 2px";
        
    }
    

    if (regex_name.test(fname.value))
    {
    document.getElementById("l2").innerHTML="Valid";
    document.getElementById("l2").style.visibility="visible" ;
    document.getElementById("l2").style.color="green";
    document.getElementById("fname").style.border="solid green 2px";
    }
    else if (fname.value.length ==0)
    {
        document.getElementById("l2").innerHTML="Father's Name can't be blank";
        document.getElementById("l2").style.visibility="visible";
        document.getElementById("l2").style.color="Red"   ;
        document.getElementById("fname").style.border="solid red 2px";
    }
    else if(regex_number.test(fname.value))
    {
        document.getElementById("l2").innerHTML="Father's Name can't contain numbers";
        document.getElementById("l2").style.visibility="visible";
        document.getElementById("l2").style.color="red";
        document.getElementById("fname").style.border="solid red 2px";
    }
    else if(regex_symbol.test(fname.value))
    {
        document.getElementById("l2").innerHTML="Father's Name can't contain symbols or special characters";
        document.getElementById("l2").style.visibility="visible";
        document.getElementById("l2").style.color="red";
        document.getElementById("fname").style.border="solid red 2px";
        
    }
    else if(regex_length.test(fname.value))
    {
        document.getElementById("l2").innerHTML="Father's Name must be greater than 2 characters";
        document.getElementById("l2").style.visibility="visible";
        document.getElementById("l2").style.color="red";
        document.getElementById("fname").style.border="solid red 2px";
        
    }
    else if(regex_length1.test(fname.value))
    {
        document.getElementById("l2").innerHTML="Father's Name must be lesser than 20 characters";
        document.getElementById("l2").style.visibility="visible";
        document.getElementById("l2").style.color="red";
        document.getElementById("fname").style.border="solid red 2px";
        
    }
    else 
    {
        document.getElementById("l2").innerHTML="Invalid father's name";
        document.getElementById("l2").style.visibility="visible";
        document.getElementById("l2").style.color="red";
        document.getElementById("fname").style.border="solid red 2px";
        
    }
    
    if (regex_email.test(email.value))
    {
    document.getElementById("l3").innerHTML="Valid";
    document.getElementById("l3").style.visibility="visible" ;
    document.getElementById("l3").style.color="green";
    document.getElementById("email").style.border="solid green 2px";
        
    
    }
    else if (email.value.length==0)
    {
        document.getElementById("l3").innerHTML="Email can't be blank";
        document.getElementById("l3").style.visibility="visible";
        document.getElementById("l3").style.color="Red"   ;
        document.getElementById("email").style.border="solid red 2px";
        
        
    }
    else
    {
        document.getElementById("l3").innerHTML="Enter a valid email Address (must have @ and Domain)";
        document.getElementById("l3").style.visibility="visible";
        document.getElementById("l3").style.color="red";
        document.getElementById("email").style.border="solid red 2px";
        
    }
    if (regex_dob.test(dob.value))
    {
    document.getElementById("l4").innerHTML="Valid";
    document.getElementById("l4").style.visibility="visible" ;
    document.getElementById("l4").style.color="green";
    document.getElementById("dob").style.border="solid green 2px";
        
    
    }
    else if (dob.value.length==0)
    {
        document.getElementById("l4").innerHTML="DOB can't be blank";
        document.getElementById("l4").style.visibility="visible";
        document.getElementById("l4").style.color="Red"   ;
        document.getElementById("dob").style.border="solid red 2px";
        
    }
    else
    {
        document.getElementById("l4").innerHTML="Type a valid DOB";
        document.getElementById("l4").style.visibility="visible";
        document.getElementById("l4").style.color="red";
        document.getElementById("dob").style.border="solid red 2px";
        
    }
    if (regex_mob.test(mob.value))
    {
    document.getElementById("l5").innerHTML="Valid";
    document.getElementById("l5").style.visibility="visible" ;
    document.getElementById("l5").style.color="green";
    document.getElementById("mob").style.border="solid Green 2px";
        
    }
    else if (mob.value.length==0)
    {
        document.getElementById("l5").innerHTML="Number can't be blank";
        document.getElementById("l5").style.visibility="visible";
        document.getElementById("l5").style.color="Red"   ;
        document.getElementById("mob").style.border="solid red 2px";
        
    }
    else if (regex_length.test(mob.value)|| regex_symbol.test(mob.value))
    {
        document.getElementById("l5").innerHTML="Number can't have letters or special characters";
        document.getElementById("l5").style.visibility="visible";
        document.getElementById("l5").style.color="Red"   ;
        document.getElementById("mob").style.border="solid red 2px";
        
        
    }
    
    else 
    {
        document.getElementById("l5").innerHTML="Type a valid Number with 10 numbers";
        document.getElementById("l5").style.visibility="visible";
        document.getElementById("l5").style.color="red";
        document.getElementById("mob").style.border="solid red 2px";
        
    }
    if (regex_a1.test(a1.value))
    {
    document.getElementById("l6").innerHTML="Valid";
    document.getElementById("l6").style.visibility="visible" ;
    document.getElementById("l6").style.color="green";
    document.getElementById("a1").style.border="solid green 2px";
       
    
    }
    else if (a1.value.length==0)
    {
        document.getElementById("l6").innerHTML="Address can't be blank";
        document.getElementById("l6").style.visibility="visible";
        document.getElementById("l6").style.color="Red"   ;
        document.getElementById("a1").style.border="solid red 2px";
       
        
    }
    else
    {
        document.getElementById("l6").innerHTML="Invalid Address - must be greater than 2 char";
        document.getElementById("l6").style.visibility="visible";
        document.getElementById("l6").style.color="Green";
        document.getElementById("a1").style.border="solid red 2px";
       
    }
    if (regex_a1.test(a2.value))
    {
    document.getElementById("l7").innerHTML="Valid";
    document.getElementById("l7").style.visibility="visible" ;
    document.getElementById("l7").style.color="green";
    document.getElementById("a2").style.border="solid green 2px";
       
    
    }
    /*else if (a2.value.length==0)
    {
        document.getElementById("l7").innerHTML="Address can't be blank";
        document.getElementById("l7").style.visibility="visible";
        document.getElementById("l7").style.color="Red"   ;
        
    }*/
    else
    {
        document.getElementById("l7").innerHTML="No addressline2";
        document.getElementById("l7").style.visibility="visible" ;
        document.getElementById("l7").style.color="green";
        document.getElementById("a2").style.border="solid green 2px";
            
    }

    if (regex_city.test(city.value))
    {
    document.getElementById("l8").innerHTML="Valid";
    document.getElementById("l8").style.visibility="visible" ;
    document.getElementById("l8").style.color="green";
    document.getElementById("city").style.border="solid green 2px";
        
    }
    else if (regex_number.test(city.value)|| regex_symbol.test(city.value))
    {
        document.getElementById("l8").innerHTML="City can't have numbers or special characters";
        document.getElementById("l8").style.visibility="visible";
        document.getElementById("l8").style.color="Red"   ;
        document.getElementById("city").style.border="solid red 2px";
        //document.getElementById("city").style.border="solid red 2px";
        
        
    }
   
    else if (city.value.length==0)
    {
        document.getElementById("l8").innerHTML="City can't be blank";
        document.getElementById("l8").style.visibility="visible";
        document.getElementById("l8").style.color="Red"   ;
        document.getElementById("city").style.border="solid red 2px";
        
    }
   
    else
    {
        document.getElementById("l8").innerHTML="Invalid - City must be greater than 2 characters";
        document.getElementById("l8").style.visibility="visible";
        document.getElementById("l8").style.color="Red";
        document.getElementById("city").style.border="solid red 2px";
        
    }

    
   
    if (opt.value=="")
    {
        document.getElementById("l9").innerHTML="Select a state";
        document.getElementById("l9").style.visibility="visible";
        document.getElementById("l9").style.color="Red"   ;
        document.getElementById("state").style.border="solid red 2px";
        document.getElementById("state").value=opt.value;
        
        
    }
    else if(state.value!=" ")
    {
    document.getElementById("l9").innerHTML="Valid";
    document.getElementById("l9").style.visibility="visible" ;
    document.getElementById("l9").style.color="green";
    document.getElementById("state").style.border="solid green 2px";
    document.getElementById("state").value=state.value;
        
    
    }

    if (regex_zip.test(zip.value))
    {
    document.getElementById("l10").innerHTML="Valid";
    document.getElementById("l10").style.visibility="visible" ;
    document.getElementById("l10").style.color="green";
    document.getElementById("zip").style.border="solid green 2px";
        
    
    }
    else if (regex_length.test(zip.value)|| regex_symbol.test(zip.value))
    {
        document.getElementById("l10").innerHTML="Zip code can't contain alphabets or special characters";
        document.getElementById("l10").style.visibility="visible";
        document.getElementById("l10").style.color="Red"   ;
        document.getElementById("zip").style.border="solid red 2px";
        
    }
   
    else if (zip.value.length==0)
    {
        document.getElementById("l10").innerHTML="zip can't be blank";
        document.getElementById("l10").style.visibility="visible";
        document.getElementById("l10").style.color="Red"   ;
        document.getElementById("zip").style.border="solid red 2px";
        
    }
    
    
    else
    {
        document.getElementById("l10").innerHTML="Invalid - zip code must be 6 digits(can't start with zero )";
        document.getElementById("l10").style.visibility="visible";
        document.getElementById("l10").style.color="Red";
        document.getElementById("zip").style.border="solid red 2px";
        
    }

    if (regex_a1.test(pa1.value))
    {
    document.getElementById("l11").innerHTML="Valid";
    document.getElementById("l11").style.visibility="visible" ;
    document.getElementById("l11").style.color="green";
    document.getElementById("pa1").style.border="solid green 2px";
       
    
    }
    else if (pa1.value.length==0)
    {
        document.getElementById("l11").innerHTML="Address can't be blank";
        document.getElementById("l11").style.visibility="visible";
        document.getElementById("l11").style.color="Red"   ;
        document.getElementById("pa1").style.border="solid red 2px";
       
        
    }
    else
    {
        document.getElementById("l11").innerHTML="Invalid Address - must be greater than 2 char";
        document.getElementById("l11").style.visibility="visible";
        document.getElementById("l11").style.color="Green";
        document.getElementById("pa1").style.border="solid red 2px";
       
    }
    if (regex_a1.test(pa2.value))
    {
    document.getElementById("l12").innerHTML="Valid";
    document.getElementById("l12").style.visibility="visible" ;
    document.getElementById("l12").style.color="green";
    document.getElementById("pa2").style.border="solid green 2px";
       
    }
    /*else if (pa2.value.length==0)
    {
        document.getElementById("l12").innerHTML="Address can't be blank";
        document.getElementById("l12").style.visibility="visible";
        document.getElementById("l12").style.color="Red"   ;
        
    }*/
    else
    {
        document.getElementById("l12").innerHTML="No Address line2";
        document.getElementById("l12").style.visibility="visible";
        document.getElementById("l12").style.color="green";
        document.getElementById("pa2").style.border="solid green 2px";
        //document.getElementById("zip").style.border="solid red 2px";
       
    }

    if (regex_city.test(pcity.value))
    {
    document.getElementById("l13").innerHTML="Valid";
    document.getElementById("l13").style.visibility="visible" ;
    document.getElementById("l13").style.color="green";
    document.getElementById("pcity").style.border="solid green 2px";
        
    
    }
    else if (regex_number.test(pcity.value)|| regex_symbol.test(pcity.value))
    {
        document.getElementById("l13").innerHTML="City can't have numbers or special characters";
        document.getElementById("l13").style.visibility="visible";
        document.getElementById("l13").style.color="Red"   ;
        document.getElementById("pcity").style.border="solid red 2px";
        //document.getElementById("city").style.border="solid red 2px";
        
        
    }
   
    else if (pcity.value.length==0)
    {
        document.getElementById("l13").innerHTML="City can't be blank";
        document.getElementById("l13").style.visibility="visible";
        document.getElementById("l13").style.color="Red"   ;
        document.getElementById("pcity").style.border="solid red 2px";
        
        
    }
    else
    {
        document.getElementById("l13").innerHTML="Invalid - City must be greater than 2 char";
        document.getElementById("l13").style.visibility="visible";
        document.getElementById("l13").style.color="Red";
        document.getElementById("pcity").style.border="solid red 2px";
        
    }

    
    if (opt1.value=="")
    {
        document.getElementById("l14").innerHTML="Select a state";
        document.getElementById("l14").style.visibility="visible";
        document.getElementById("l14").style.color="Red"   ;
        document.getElementById("pstate").style.border="solid red 2px";
        document.getElementById("pstate").value=opt.value;
        
        
    }
    else if(pstate.value!=" ")
    {
    document.getElementById("l14").innerHTML="Valid";
    document.getElementById("l14").style.visibility="visible" ;
    document.getElementById("l14").style.color="green";
    document.getElementById("pstate").style.border="solid green 2px";
    document.getElementById("pstate").value=pstate.value;
        
    
    }


    if (regex_zip.test(pzip.value))
    {
    document.getElementById("l15").innerHTML="Valid";
    document.getElementById("l15").style.visibility="visible" ;
    document.getElementById("l15").style.color="green";
    document.getElementById("pzip").style.border="solid green 2px";
       
    
    }
    else if (regex_length.test(pzip.value)|| regex_symbol.test(pzip.value))
    {
        document.getElementById("l15").innerHTML="Zip code can't contain alphabets or special characters";
        document.getElementById("l15").style.visibility="visible";
        document.getElementById("l15").style.color="Red"   ;
        document.getElementById("pzip").style.border="solid red 2px";
        
    }
   
    else if (pzip.value.length==0)
    {
        document.getElementById("l15").innerHTML="zip can't be blank";
        document.getElementById("l15").style.visibility="visible";
        document.getElementById("l15").style.color="Red"   ;
        document.getElementById("pzip").style.border="solid red 2px";
       
    }
    else
    {
        document.getElementById("l15").innerHTML="Invalid - zip code must be 6 digits(can't start with zero )";
        document.getElementById("l15").style.visibility="visible";
        document.getElementById("l15").style.color="Red";
        document.getElementById("pzip").style.border="solid red 2px";
       
    }
    if (im.value != "")
    {
        document.getElementById("l16").style.visibility="visible";
        document.getElementById("l16").style.color="green"
        document.getElementById("l16").innerHTML="Image Added";
        document.getElementById("pic").style.border="solid green 2px";
       
        
    }
    else
    {
        document.getElementById("l16").innerHTML="Image is required. Choose an Image";
        document.getElementById("l16").style.visibility="visible";
        document.getElementById("l16").style.color="Red";
        document.getElementById("pic").style.border="solid red 2px";
       
    }

   
}

function copyaddress()
    {
        var che=document.getElementById("che");
        if(che.checked==true)
        {
        var a1=document.getElementById("a1");
    var a2=document.getElementById("a2");
    var city = document.getElementById("city");
    var state= document.getElementById("state");
    var zip =document.getElementById("zip");
    
        document.getElementById("pa1").value=a1.value;
        document.getElementById("pa2").value=a2.value;
        document.getElementById("pcity").value=city.value;
        document.getElementById("pstate").value=state.value;
        document.getElementById("pzip").value=zip.value;
        }
}